<?php 

require("animal.php");
require("frog.php");
require("ape.php");

$sheep = new animal("shaun");
echo "Nama Hewan : $sheep->name <br>";
echo "Legs : $sheep->legs <br> ";
echo "Cold blooded : $sheep->cold_blooded <br><br>";

$kodok = new frog("buduk");
echo "Nama Hewan : $kodok->name <br>";
echo "Legs : $kodok->legs <br> ";
echo "Cold blooded : $kodok->cold_blooded <br>";
echo $kodok->yell();
echo "<br><br>";

$sungokong = new ape("kera sakti");
echo "Nama Hewan : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br> ";
echo "Cold blooded : $sungokong->cold_blooded <br>";
echo $sungokong->jump();

?>